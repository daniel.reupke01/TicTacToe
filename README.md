# TicTacToe mit UR10

Dieses Repsositorium ermöglicht es TicTacToe gegen den Roboter UR10 zu spielen. Das Repositorium beinhaltet zwei verschiedene Umsetzungen: 
- RealSense: Bei dieser Gruppe und Ordner wird das Kamerabild mithilfe einer Realsense-Kamera von Intel gewonnen. Innerhalb eines Pythonskriptes wird dieses in eine Spielfeldmatrix umgewandelt und eine Spielzuglogik berechnet den Zug des Computers. Auch Bewegungen des Roboters, bei denen es sich immer um das Bewegen von Spielsteinen handelt ist mithilfe von Python realisiert. 
- RobotIq: hier wird die Positionserkennung der Spielsteine mit Hilfe eines UR-Caps umgesetz das selbst auf der RobotIq-Kamera beruht. Ein UR-Programm ließt dabei wiederkehrend das Spielfeld aus und veröffentlicht dieses als globale Variable innerhalb eines übergeordneten UR-Programms. Ein weiteres UR-Skript berechnet den nächsten Zug des Computers. 
- Server: Hier handelt es sich um zwei Server die ähnlich von beiden Gruppen genutzt werden. Zum einen ist das das Dashboard, auf dem der User das Spiel Starten, Abbrechen und Aufräumen kann. Zum anderen handelt es sich um einen Server der die Inhalte des Fernsehers darstellt. 

Folgend wird berschrieben, wie das Projekt eingerichtet wird. 

# Development Setup and Configuration
blablabla